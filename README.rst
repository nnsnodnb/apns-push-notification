APNs-Push-Notification
======================

Environment
-----------

-  Python 2.7.11

Installation
------------

.. code:: bash

    $ pip install -r requirements.txt

Description
~~~~~~~~~~~

| ``cert_file`` is Public key. ``key_file`` is Secret key.
| Convert p12 file to pem file.

.. code:: bash

    $ openssl pkcs12 -in secret.p12 -out secret.pem -nodes -clcerts
    $ openssl pkcs12 -in public.p12 -out public.pem -nodes -clcerts

.. code:: python:push.py

    apns = APNs(use_sandbox=True, cert_file='public.pem', key_file='secret.pem')

Rewrite each as described above code.

Support Development and Distribute APNs.
