# coding: utf-8

import time
from apns import APNs, Frame, Payload

apns = APNs(use_sandbox=False, cert_file='pushCer.pem', key_file='pushkey.pem', enhanced=True)

token_hex = ['417a7b3329c86240b159b816313a3ae51ff9059b98ac29bc7cbcbd36dd41e2a9',
             '446d18e7a621d371a8683d0d33bf9080091efbf693f5b591f9ec307a1e627d46']

message = "メッセージ！！"
message = message.decode('utf-8')

payload = Payload(alert=message, sound="default", badge=0, custom={'uri': 'https://dl.nnsnodnb.moe'})

frame = Frame()
identifier = 1
expiry = time.time() + 3600
priority = 10

for token in token_hex:
    frame.add_item(token, payload, identifier, expiry, priority)

apns.gateway_server.send_notification_multiple(frame)
