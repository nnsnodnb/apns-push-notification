# coding: utf-8

import time
from apns import APNs, Frame, Payload

apns = APNs(use_sandbox=True, cert_file='pushCerDev.pem', key_file='pushkey.pem')

token_hex = '45f9f318ac6506742a54a3b8bb6493c72cdad9afe2c154d5295e9db15cdf83a7'

content = "テスト!!"
content = content.decode('utf-8')

payload = Payload(alert=content, sound="default", badge=0)
apns.gateway_server.send_notification(token_hex, payload)
